//
//  EditWonderViewController.swift
//  MyWonders
//
//  Created by Mikhail Bhuta on 4/18/16.
//  Copyright © 2016 Mikhail Bhuta. All rights reserved.
//

import UIKit
import CoreData

class EditWonderViewController: UIViewController {
    
    var timer = NSTimer()
    var textColor = UIColor.blackColor()
    
    @IBOutlet var editConfirmationLabel: UILabel!
    @IBOutlet var wonderNameTextField: UITextField!
    @IBOutlet var latitudeTextField: UITextField!
    @IBOutlet var longitudeTextField: UITextField!
    @IBOutlet var notesTextView: UITextView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.textColor = editConfirmationLabel.textColor
        
        wonderNameTextField.text = editSelectedWonderName
        
        let latVal:Double = editSelectedWonderLatitude
        let latString = String(format: "%.6f", latVal)
        
        let lonVal:Double = editSelectedWonderLongitude
        let lonString = String(format: "%.6f", lonVal)
        
        latitudeTextField.text = latString
        longitudeTextField.text = lonString
        
        notesTextView.text = editSelectedWonderNotes
        notesTextView.layer.borderWidth = 0.5
        notesTextView.layer.borderColor = UIColor.lightGrayColor().CGColor
        
        editConfirmationLabel.alpha = 0
        
        let saveRightBarButton = UIBarButtonSystemItem.Save
        navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: saveRightBarButton, target: self, action: #selector(EditWonderViewController.editSaveButtonAction(_:)))
        
    }
    
    @IBAction func editSaveButtonAction(sender: UIBarButtonItem)
    {
        var wonders = [Wonders]()
        
        let wondersAppDel: AppDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        let wondersContext: NSManagedObjectContext = wondersAppDel.managedObjectContext
        let wondersFetchRequest = NSFetchRequest(entityName: "Wonders")
        do
        {
            if let wonderFetchedResults = try wondersContext.executeFetchRequest(wondersFetchRequest) as? [Wonders]
            {
                wonders = wonderFetchedResults
            }
            else
            {
                print("If let wonderFetchedResults = try...FAILED")
            }
        }
        catch
        {
            fatalError("There was an error fetching the list of wonders")
        }
        
        print("Editing row at indexPath.row \(editSelectedRow)")
        let wonder = wonders[editSelectedRow]
        print(wonders)
        
        wonder.wonderName = wonderNameTextField.text!
        wonder.wonderLatitude = Double(latitudeTextField.text!) ?? 0.0
        wonder.wonderLongitutde = Double(longitudeTextField.text!) ?? 0.0
        wonder.wonderNotes = notesTextView.text
        
        do
        {
            try wondersContext.save()
            editConfirmationLabel.text = "Saved " + wonderNameTextField.text!
            editConfirmationLabel.textColor = self.textColor
            self.editConfirmationLabel.alpha = 1
            self.timer = NSTimer.scheduledTimerWithTimeInterval(0.2, target: self, selector: #selector(EditWonderViewController.timerAction), userInfo: nil, repeats: false)
        }
        catch
        {
            editConfirmationLabel.text = "Save failed: \(error)"
            editConfirmationLabel.textColor = UIColor.redColor()
            editConfirmationLabel.alpha = 1
            print("Save failed: \(error)")
        }
    }
    
    func timerAction()
    {
        UIView.animateWithDuration(1.0) {
            self.editConfirmationLabel.alpha = 0
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
