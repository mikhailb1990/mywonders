//
//  PhotosViewController.swift
//  MyWonders
//
//  Created by Mikhail Bhuta on 4/18/16.
//  Copyright © 2016 Mikhail Bhuta. All rights reserved.
//

import UIKit
import CoreData
import AVFoundation
import MobileCoreServices

class PhotosViewController: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    var img:Photos!
    
    var photosWonderName: String!
    var photosSourceType: String!
    var timer = NSTimer()

    @IBOutlet var addImageToLabel: UILabel!
    @IBOutlet var addImageSwitch: UISwitch!
    @IBOutlet var saveConfirmationLabel: UILabel!
    @IBOutlet var imageView: UIImageView!
    
    @IBAction func addWonderPhotoAction(sender: AnyObject) {
        
        accessCameraOrPhotoLibrary()
        
    }
    
    @IBAction func addImageToCoreDataAction(sender: AnyObject) {
        
        addImageToCoreData()
        
    }
    
    func timerAction()
    {
        UIView.animateWithDuration(1.0) {
            self.saveConfirmationLabel.alpha = 0
        }
    }

    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        addImageToLabel.text = photosWonderName
        addImageSwitch.alpha = 0
        saveConfirmationLabel.alpha = 0
        
        accessCameraOrPhotoLibrary()
      
    }
    
    func accessCameraOrPhotoLibrary()
    {
        let imagePicker = UIImagePickerController()
        imagePicker.delegate = self
        
        if photosSourceType == "Photos"
        {
            if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.PhotoLibrary)
            {
                imagePicker.sourceType = UIImagePickerControllerSourceType.PhotoLibrary
                imagePicker.mediaTypes = [kUTTypeImage as NSString as String]
                presentViewController(imagePicker, animated: true, completion: nil)
            }
        }
    }
    
    func addImageToCoreData()
    {
        let photosAppDel = UIApplication.sharedApplication().delegate as! AppDelegate
        let photosContext = photosAppDel.managedObjectContext
        
        if addImageSwitch.on
        {
            // SETUP IMAGE FOR CORE DATA
            let newImageData = UIImageJPEGRepresentation(imageView.image!, 1)   // Binary data object of photo image
            let newPhoto = NSEntityDescription.insertNewObjectForEntityForName("Photos", inManagedObjectContext: photosContext) as! Photos
            newPhoto.wonderName = photosWonderName
            newPhoto.wonderPhoto = newImageData
            
            do
            {
                try photosContext.save()
                saveConfirmationLabel.text = "Saved photo of " + photosWonderName
                self.saveConfirmationLabel.alpha = 1
                self.timer = NSTimer.scheduledTimerWithTimeInterval(0.4, target: self, selector: #selector(PhotosViewController.timerAction), userInfo: nil, repeats: false)
            }
            catch
            {
                saveConfirmationLabel.alpha = 1
                saveConfirmationLabel.text = "Save failed: \(error)"
                print("Save failed: \(error)")
            }
        }
    }
    
    func imagePickerControllerDidCancel(picker: UIImagePickerController) {
        
        dismissViewControllerAnimated(true, completion: nil)
        
    }
    
    func imagePickerController(picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : AnyObject]) {
        
        dismissViewControllerAnimated(true, completion: nil)
        
        let mediaType = info[UIImagePickerControllerMediaType] as! NSString
        
        if mediaType.isEqualToString(kUTTypeImage as NSString as String)
        {
            let image = info[UIImagePickerControllerOriginalImage] as! UIImage
            imageView.image = image
            imageView.contentMode = .ScaleAspectFit
        }
        
        addImageToLabel.alpha = 1
        addImageSwitch.alpha = 1
        addImageSwitch.setOn(true, animated: true)
        saveConfirmationLabel.alpha = 0
        
        addImageToCoreData()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
