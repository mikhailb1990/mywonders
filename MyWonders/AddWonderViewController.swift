//
//  AddWonderViewController.swift
//  MyWonders
//
//  Created by Mikhail Bhuta on 4/14/16.
//  Copyright © 2016 Mikhail Bhuta. All rights reserved.
//

import UIKit
import CoreData // May not need this import? Delete and see after writing code

class AddWonderViewController: UIViewController, UITextFieldDelegate {
    
    var wonderName: String = ""
    var latitudeVal: Double = 0.0
    var longitudeVal: Double = 0.0
    var notesTxt: String = ""
    var timer = NSTimer()

    @IBOutlet var saveConfirmationLabel: UILabel!
    @IBOutlet var wonderNameTextField: UITextField!
    @IBOutlet var latitudeTextField: UITextField!
    @IBOutlet var longitudeTextField: UITextField!
    @IBOutlet var notesTextView: UITextView!
    @IBOutlet var photosButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        wonderNameTextField.delegate = self
        let saveRightBarButton = UIBarButtonSystemItem.Save
        navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: saveRightBarButton, target: self, action: #selector(AddWonderViewController.addSaveButtonAction(_:)))
        
        saveConfirmationLabel.alpha = 0
        notesTextView.layer.borderWidth = 0.5
        notesTextView.layer.borderColor = UIColor.lightGrayColor().CGColor
        notesTextView.text = "Notes..."
        photosButton.enabled = false
    }
    
    @IBAction func addSaveButtonAction(sender: UIBarButtonItem)
    {
        wonderName = wonderNameTextField.text!
        latitudeVal = Double(latitudeTextField.text!) ?? 0.0
        longitudeVal = Double(longitudeTextField.text!) ?? 0.0
        notesTxt = notesTextView.text!
        
        // Save wonder to Core Data
        let wonderAppDel: AppDelegate = UIApplication.sharedApplication().delegate as! AppDelegate  // Grab the app delegate
        let wondersContext: NSManagedObjectContext = wonderAppDel.managedObjectContext  // Grab the context: scrapbook for storing temp data
        
        // Create the new wonder which will be an NSManaged object, or a subclass of it
        let newWonder = NSEntityDescription.insertNewObjectForEntityForName("Wonders", inManagedObjectContext: wondersContext) as! Wonders
        newWonder.wonderName = wonderName
        newWonder.wonderLatitude = latitudeVal
        newWonder.wonderLongitutde = longitudeVal
        newWonder.wonderNotes = notesTxt
        newWonder.wonderShow = true
        newWonder.wonderType = "MY"
        
        // Save the object
        do
        {
            try wondersContext.save()
            saveConfirmationLabel.text = "Saved " + wonderName
            self.saveConfirmationLabel.alpha = 1
            self.timer = NSTimer.scheduledTimerWithTimeInterval(0.2, target: self, selector: #selector(AddWonderViewController.timerAction), userInfo: nil, repeats: false)
            photosButton.enabled = true
        } catch
        {
            saveConfirmationLabel.alpha = 1
            saveConfirmationLabel.text = "Save failed: \(error)"
            print("Save failed: \(error)")
        }
        
    }
    
    func timerAction()
    {
        UIView.animateWithDuration(1.0) {
            self.saveConfirmationLabel.alpha = 0
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // Keyboard functions
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        wonderNameTextField.resignFirstResponder()
        return false
    }
    
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        
        if segue.identifier == "addToPhotos"
        {
            let vc = segue.destinationViewController as! PhotosViewController
            vc.photosWonderName = wonderName
            vc.photosSourceType = "Photos"
        }
        
    }
    

}
