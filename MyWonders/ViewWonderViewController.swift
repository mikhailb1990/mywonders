//
//  ViewWonderViewController.swift
//  MyWonders
//
//  Created by Mikhail Bhuta on 4/15/16.
//  Copyright © 2016 Mikhail Bhuta. All rights reserved.
//

import UIKit
import MapKit
import CoreLocation
import CoreData

class ViewWonderViewController: UIViewController, MKMapViewDelegate, CLLocationManagerDelegate {

    @IBOutlet var wonderNameLabel: UILabel!
    @IBOutlet var latValLabel: UILabel!
    @IBOutlet var lonValLabel: UILabel!
    @IBOutlet var notesTextView: UITextView!
    @IBOutlet var mapView: MKMapView!
    @IBOutlet var photosButton: UIButton!
    @IBOutlet var numPhotosLabel: UILabel!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
        wonderNameLabel.text = viewSelectedWonderName
        
        let latVal:Double = viewSelectedWonderLatitude
        let latString = String(format: "%.6f", latVal)
        
        let lonVal:Double = viewSelectedWonderLongitude
        let lonString = String(format: "%.6f", lonVal)
        
        latValLabel.text = latString
        lonValLabel.text = lonString
        
        notesTextView.text = viewSelectedWonderNotes
        
        // Map setup
        let latitude: CLLocationDegrees = latVal
        let longitude: CLLocationDegrees = lonVal
        
        let deltaLat: CLLocationDegrees = 0.01
        let deltaLon: CLLocationDegrees = 0.01
        
        let span: MKCoordinateSpan = MKCoordinateSpanMake(deltaLat, deltaLon)
        
        let location: CLLocationCoordinate2D = CLLocationCoordinate2DMake(latitude, longitude)
        
        let region: MKCoordinateRegion = MKCoordinateRegionMake(location, span)
        
        mapView.setRegion(region, animated: true)
        
        // Map annotations
        let annotation = MKPointAnnotation()
        annotation.coordinate = location
        annotation.title = viewSelectedWonderName
        
        mapView.addAnnotation(annotation)
        
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        let photosAppDel = UIApplication.sharedApplication().delegate as! AppDelegate
        let photosContext = photosAppDel.managedObjectContext
        let photosFetchRequest = NSFetchRequest(entityName: "Photos")
        photosFetchRequest.predicate = NSPredicate(format: "wonderName = %@", viewSelectedWonderName)
        
        var photos:[Photos] = []
        
        do
        {
            let results = try photosContext.executeFetchRequest(photosFetchRequest) as? [Photos]
            photos = results!
        }
        catch
        {
            print("Could not fetch photos")
        }
        
        numPhotosLabel.text = String(photos.count)
        
        if photos.count == 0
        {
            if let image = UIImage(named: "photos")
            {
                photosButton.setImage(image, forState: .Normal)
            }
        }
        else
        {
            let photo = photos[0]
            if let thumbnail = UIImage(data: photo.wonderPhoto!)
            {
                photosButton.setImage(thumbnail, forState: .Normal)
            }
            else
            {
                if let image = UIImage(named: "photos")
                {
                    photosButton.setImage(image, forState: .Normal)
                }
            }
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
